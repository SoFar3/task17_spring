package com.epam.view;

import com.epam.view.menu.Menu;
import com.epam.view.menu.Option;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class ViewImpl implements View {

    private Menu menu;
    private BufferedReader reader;

    public ViewImpl() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    @Override
    public void showMenu() {
        //int counter = 1;
        while (true) {
            System.out.println();
            for (Map.Entry<String, Option> stringOptionEntry : menu.getMenu().entrySet()) {
                View.print(String.format("\t%s - %s", stringOptionEntry.getKey(), stringOptionEntry.getValue().getName()), ConsoleColor.RESET);
            }
            System.out.println();
            waitForUserOption();
            if (menu.getRepeat() == 0) {
                if (menu.getMenu().get("N") != null) {
                    menu.getMenu().get("N").getAction().run();
                } else {
                    menu.getMenu().get("Q").getAction().run();
                }
            }
            menu.setRepeat(menu.getRepeat() - 1);
        }
    }

    @Override
    public String getString(String message) {
        String input = "";
        try {
            System.out.println(message);
            input = reader.readLine();
        } catch (IOException ignored) { }
        return input;
    }

    @Override
    public Integer getInteger(String message) {
        while (true) {
            try {
                return Integer.parseInt(getString(message));
            }
            catch (NumberFormatException e) {
                View.print("This is not an integer. Try again...", ConsoleColor.RED);
            }
        }
    }

    @Override
    public void waitForUserOption() {
        System.out.println("Enter your choice:");
        while (true) {
            try {
                String choice = reader.readLine();
                if (doAction(choice)) {
                    break;
                }
                System.out.println("Incorrect input");
            } catch (IOException ignored) { }
        }
    }

    @Override
    public boolean doAction(String key) {
        Option option = menu.getMenu().get(key);
        if (option == null) {
            return false;
        } else {
            option.getAction().run();
            return true;
        }
    }

}
