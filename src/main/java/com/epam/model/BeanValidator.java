package com.epam.model;

public interface BeanValidator {

    boolean validate();

}
