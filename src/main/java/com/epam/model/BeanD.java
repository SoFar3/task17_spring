package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

public class BeanD implements BeanValidator {

    @Value("${beanD.name}")
    private String name = "";

    @Value("${beanD.value}")
    private int value = 0;

    private static final Logger logger = LogManager.getLogger();

    public BeanD() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void init() {
        logger.debug("BeanD initialization");
    }

    public void destroy() {
        logger.debug("Destroying BeanD");
    }

    @Override
    public boolean validate() {
        return name != null && value >= 0;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
