package com.epam.model.beans2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

public class RoseFlower {

    private String name;
    private int value;

    private static final Logger logger = LogManager.getLogger();

    public RoseFlower() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "RoseFlower{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
