package com.epam.model;

import org.springframework.beans.BeanInstantiationException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class ValidationBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof BeanValidator) {
            if (((BeanValidator)bean).validate()) {
                return bean;
            }
            throw new BeanInstantiationException(bean.getClass(), beanName + ": name value must be not null and value must be positive");
        }
        return bean;
    }

}
