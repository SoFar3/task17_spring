package com.epam.model.otherbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Qualifier("aloneInTheDark")
public class MarkBean2 implements Mark {

    @Override
    public String toString() {
        return "MarkBean2{}";
    }

}
