package com.epam.model.otherbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MarkContainerBean {

    private List<Mark> markList;

    @Autowired
    private Mark primaryMark;

    @Autowired
    @Qualifier("aloneInTheDark")
    private Mark aloneMark;

    public MarkContainerBean(List<Mark> markList) {
        this.markList = markList;
    }

    @Override
    public String toString() {
        return "MarkContainerBean{" +
                "markList=" + markList +
                ", primaryMark=" + primaryMark +
                ", aloneMark=" + aloneMark +
                '}';
    }

}
