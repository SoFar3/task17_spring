package com.epam.model.otherbeans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("qualifiedBean")
@Scope("prototype")
public class OtherBeanC {

    private String name;
    private int value;

    public OtherBeanC() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "OtherBeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
