package com.epam.model.otherbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class AutowiredBean {

    @Autowired
    private OtherBeanA otherBeanA;

    private OtherBeanB otherBeanB;

    private OtherBeanC otherBeanC;

    @Autowired
    public AutowiredBean(OtherBeanB otherBeanB) {
        this.otherBeanB = otherBeanB;
    }

    public String hashesToString() {
        return otherBeanA.hashCode() + " " + otherBeanB.hashCode() + " " + otherBeanC.hashCode();
    }

    public OtherBeanA getOtherBeanA() {
        return otherBeanA;
    }

    public void setOtherBeanA(OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;
    }

    public OtherBeanB getOtherBeanB() {
        return otherBeanB;
    }

    public void setOtherBeanB(OtherBeanB otherBeanB) {
        this.otherBeanB = otherBeanB;
    }

    public OtherBeanC getOtherBeanC() {
        return otherBeanC;
    }

    @Autowired
    public void setOtherBeanC(@Qualifier("qualifiedBean") OtherBeanC otherBeanC) {
        this.otherBeanC = otherBeanC;
    }

    @Override
    public String toString() {
        return "AutowiredBean{" +
                "otherBeanA=" + otherBeanA +
                ", otherBeanB=" + otherBeanB +
                ", otherBeanC=" + otherBeanC +
                '}';
    }

}
