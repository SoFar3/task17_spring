package com.epam.model.otherbeans;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
@Primary
public class MarkBean1 implements Mark {

    @Override
    public String toString() {
        return "MarkBean1{}";
    }

}
