package com.epam.model.beans3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanE {

    private String name;
    private int value;

    private static final Logger logger = LogManager.getLogger();

    public BeanE() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
