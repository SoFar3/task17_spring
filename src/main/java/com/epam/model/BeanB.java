package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

public class BeanB implements BeanValidator {

    @Value("${beanB.name}")
    private String name = "";

    @Value("${beanB.value}")
    private int value = 0;

    private static final Logger logger = LogManager.getLogger();

    public BeanB() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void init() {
        logger.debug("BeanB initialization");
    }

    public void secretInit() {
        logger.debug("Super secret BeanB initialization");
    }

    public void destroy() {
        logger.debug("Destroying BeanB");
    }

    @Override
    public boolean validate() {
        return name != null && value >= 0;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
