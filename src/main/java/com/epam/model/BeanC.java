package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

public class BeanC implements BeanValidator {

    @Value("${beanC.name}")
    private String name = "";

    @Value("${beanC.value}")
    private int value = 0;

    private static final Logger logger = LogManager.getLogger();

    public BeanC() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void init() {
        logger.debug("BeanC initialization");
    }

    public void destroy() {
        logger.debug("Destroying BeanC");
    }

    @Override
    public boolean validate() {
        return name != null && value >= 0;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

}
