package com.epam.config.part2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(
        basePackages = {"com.epam.model.beans2", "com.epam.model.beans3"},
        useDefaultFilters = false,
        includeFilters = {
                @ComponentScan.Filter(type = FilterType.REGEX, pattern = ".*Flower"),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {
                        com.epam.model.beans3.BeanD.class,
                        com.epam.model.beans3.BeanF.class
                })
        })
public class ConfigTwo {



}
