package com.epam.config.part2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.epam.model.beans1", "com.epam.model.otherbeans"})
public class ConfigOne {

}
