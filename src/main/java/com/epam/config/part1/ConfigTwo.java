package com.epam.config.part1;

import com.epam.model.BeanB;
import com.epam.model.BeanC;
import com.epam.model.BeanD;
import com.epam.model.BeanF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;

@Configuration
public class ConfigTwo {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public BeanB getBeanB() {
        return new BeanB();
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @DependsOn({"getBeanD", "getBeanB"})
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD() {
        return new BeanD();
    }

    @Bean
    @Lazy
    public BeanF getBeanF() {
        BeanF beanF = new BeanF();
        beanF.setName("BeanF");
        beanF.setValue(6);
        return beanF;
    }

}
