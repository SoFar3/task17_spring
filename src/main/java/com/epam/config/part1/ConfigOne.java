package com.epam.config.part1;

import com.epam.model.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.*;

@Configuration
@Import(ConfigTwo.class)
@PropertySource("/messages.properties")
public class ConfigOne {

    @Bean
    public BeanA getBeanAfromBC(BeanB beanB, BeanC beanC) {
        BeanA beanA = new BeanA();
        beanA.setName(beanB.getName());
        beanA.setValue(beanC.getValue());
        return beanA;
    }

    @Bean
    public BeanA getBeanAfromBD(BeanB beanB, BeanD beanD) {
        BeanA beanA = new BeanA();
        beanA.setName(beanB.getName());
        beanA.setValue(beanD.getValue());
        return beanA;
    }

    @Bean
    public BeanA getBeanAfromCD(BeanC beanC, BeanD beanD) {
        BeanA beanA = new BeanA();
        beanA.setName(beanC.getName());
        beanA.setValue(beanD.getValue());
        return beanA;
    }

    @Bean("beanEone")
    public BeanE getBeanEone(@Qualifier("getBeanAfromBC") BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setName(beanA.getName());
        beanE.setValue(beanA.getValue());
        return beanE;
    }

    @Bean("beanEtwo")
    public BeanE getBeanEtwo(@Qualifier("getBeanAfromBD") BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setName(beanA.getName());
        beanE.setValue(beanA.getValue());
        return beanE;
    }

    @Bean("beanEthree")
    public BeanE getBeanEthree(@Qualifier("getBeanAfromCD") BeanA beanA) {
        BeanE beanE = new BeanE();
        beanE.setName(beanA.getName());
        beanE.setValue(beanA.getValue());
        return beanE;
    }

    @Bean
    public BeanPostProcessor validationBeanPostProcessor() {
        return new ValidationBeanPostProcessor();
    }

    @Bean
    public static BeanFactoryPostProcessor customBeanFactoryPostProcessor() {
        return new CustomBeanFactoryPostProcessor();
    }

}
