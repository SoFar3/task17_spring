package com.epam.controller;

import com.epam.model.otherbeans.AutowiredBean;
import com.epam.model.otherbeans.MarkContainerBean;
import com.epam.view.View;
import com.epam.view.ViewImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ControllerImpl implements Controller {

    private static final Logger logger = LogManager.getLogger();

    private View view;

    public ControllerImpl() {
        view = new ViewImpl();
    }

    @Override
    public void start() {
        tasksPart1();
        tasksPart2();
    }

    private void tasksPart1() {
        ApplicationContext context = new AnnotationConfigApplicationContext(com.epam.config.part1.ConfigOne.class);

        com.epam.model.BeanC c = context.getBean(com.epam.model.BeanC.class);
        com.epam.model.BeanB b = context.getBean(com.epam.model.BeanB.class);
        com.epam.model.BeanD d = context.getBean(com.epam.model.BeanD.class);

        logger.debug(b);
        logger.debug(c);
        logger.debug(d);

        com.epam.model.BeanA aFromBC = context.getBean("getBeanAfromBC", com.epam.model.BeanA.class);
        com.epam.model.BeanA aFromBD = context.getBean("getBeanAfromBD", com.epam.model.BeanA.class);
        com.epam.model.BeanA aFromCD = context.getBean("getBeanAfromCD", com.epam.model.BeanA.class);

        logger.debug(aFromBC);
        logger.debug(aFromBD);
        logger.debug(aFromCD);

        com.epam.model.BeanE eOne = context.getBean("beanEone", com.epam.model.BeanE.class);
        com.epam.model.BeanE eTwo = context.getBean("beanEtwo", com.epam.model.BeanE.class);
        com.epam.model.BeanE eThree = context.getBean("beanEthree", com.epam.model.BeanE.class);

        logger.debug(eOne);
        logger.debug(eTwo);
        logger.debug(eThree);
    }

    private void tasksPart2() {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(com.epam.config.part2.ConfigOne.class, com.epam.config.part2.ConfigTwo.class);

        AutowiredBean autowiredBean0 = context.getBean(AutowiredBean.class);
        AutowiredBean autowiredBean1 = context.getBean(AutowiredBean.class);

        MarkContainerBean markContainerBean = context.getBean(MarkContainerBean.class);

        logger.debug(autowiredBean0);
        logger.debug(autowiredBean0.hashesToString());
        logger.debug(autowiredBean1.hashesToString());

        logger.debug(markContainerBean);
    }

}
